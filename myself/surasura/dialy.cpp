// 日記のプログラム
#include<stdio.h>

int main(int argc, char const *argv[])
{
    // FILE構造体のポインタを作る
    FILE *f;

    struct day{
        char weather[30];
        int temperature;
    };

    struct body{
        float length;
        float weight;
        float bmi;

        void
        bmi_calc(){  // BIM値を計算する関数
            float length_m = length/100;  // [m]にする
            bmi = weight / length_m / length_m;
        }
    };

    struct day today;
    struct body me;

    printf("今日の天気を入力してください\n");
    scanf("%s", today.weather);
    printf("今日の気温を整数で入力してください\n");
    scanf("%d", &today.temperature);

    printf("今日の身長を実数で入力してください\n");
    scanf("%f", &me.length);
    printf("今日の体重を実数で入力してください\n");
    scanf("%f", &me.weight);

    f = fopen("test.txt", "a");

    // 例外処理
    if ( f == NULL ){
        printf("ファイルが開けませんでした\n");
        return 1;
    }
    printf("ファイルを開きました\n");

    // 実際にファイルに書き込む
    fprintf(f,"今日の天気は%sでした。\n", today.weather);
    if (today.temperature > 30) fprintf(f, "今日は暑くて学校行きた行くない\n");
    else fprintf(f, "学校行くか〜\n");
    me.bmi_calc();  //BMIを計算する
    fprintf(f, "今のBMI数値は%.2fだ\n", me.bmi);
    if (me.bmi > 25.0) fprintf(f, "太りすぎかな\n");
    else fprintf(f, "まぁ大丈夫でしょ\n");

    fclose(f);
    printf("ファイルを閉じました\n");
    
    
    return 0;
}
