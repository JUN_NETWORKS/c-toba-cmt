// ポインタ
#include <stdio.h>

int main(int argc, char const *argv[])
{

    // ポインタを宣言
    int *pa;
    
    int a = 10;

    // アドレス番号を入れる
    pa = &a;

    // メモリのアドレス表示
    printf("pa: %p\n",pa);
    printf("a: %p\n", &a);

    // メモリのアドレス内の情報を表示
    printf("pa content: %d\n", *pa);

    return 0;
}
