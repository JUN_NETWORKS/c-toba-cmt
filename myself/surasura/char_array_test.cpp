#include <stdio.h>

// 文字列を標準出力するプログラム
int main(void){
    // char a[6] = "abcde";
    char a[] = "abcde";  // こうすると文字数に関係なく変数に入れることができる
    printf("%s\n",a);

    printf("%sの3文字目は%cです\n",a,a[2]); // %s: 文字列, %c: 1文字

    // forで1文字ずつ取り出すことも出来る
    for (int i=0;i<=5;i++){
        printf("%c ",a[i]);
    }


    // 標準入力で文字列を受け取る
    printf("\n文字列を入力してください\n");

    char b[50];  // 最後に終端文字が入るので実質1バイト文字が49文字まで入る
    scanf("%s",b); // メモリ上のアドレスは既に確保してあるので&はいらない
    printf("入力された文字列: %s\n",b);

    return 0;
}