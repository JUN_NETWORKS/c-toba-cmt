#include<stdio.h>

int main(int argc, char const *argv[])
{
    // 構造体
    struct today{
        char tenki[10];
        int kion;
    };

    // 宣言
    struct today kyou;
    struct today ashita;

    kyou.kion = 32;

    printf("今日の天気を入力してください。\n");
    scanf("%s", kyou.tenki);

    printf("今日の天気は%sです\n", kyou.tenki);
    printf("今日の気温は%d度です\n", kyou.kion);

    // 構造体をコピーする
    ashita = kyou;

    printf("明日の天気も%sですか。\n", ashita.tenki);
    return 0;
}
