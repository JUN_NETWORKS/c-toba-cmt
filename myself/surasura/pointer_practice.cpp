#include<stdio.h>

// 変数の中身を入れ替える関数
int change(int *a, int *b){
    int ichiji;

    // アドレス内の数値を書き換えることで間接的に変数の内容を書き換えている
    ichiji = *a;
    *a = *b;
    *b = ichiji;
    return 0;
}

int main(int argc, char const *argv[])
{
    
    int a = 100;
    int b = 0;

    printf("before : %d %d\n",a,b);

    // 引数としてアドレスを渡す
    change(&a,&b);
    
    printf("after : %d %d\n",a,b);
    return 0;
}
