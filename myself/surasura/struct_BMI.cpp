#include<stdio.h>

// 型名に別名をつける(元の struct body)
typedef struct body{
    float shincho;
    float taiju;
    float bmi;
}bo;

int main(int argc, char const *argv[])
{
    // struct body{
    //     float shincho;
    //     float taiju;
    //     float bmi;
    // }suzuki, tanaka;  // 構造体の定義と同時に宣言もできる

    // 宣言と同時に代入する
    // struct body me = { 165.0, 65.0 };
    
    // 別名を付けたので短い名前で宣言できる
    bo me = { 165.0, 65.0 };

    printf("私の身長は%.2fcmです。\n", me.shincho);
    printf("体重は%.2fです\n", me.taiju);

    me.shincho = me.shincho /100;
    me.bmi = me.taiju / me.shincho / me.shincho;

    printf("BMI: %f.2\n", me.bmi);
    
    return 0;
}
