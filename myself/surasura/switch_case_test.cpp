#include <stdio.h>

int main(void){

    int a=0;

    printf("Switch Test\n");
    printf("A:1, B:2, C:3\n");

    scanf("%d",&a);

    switch(a){
        case 1:
            printf("This is A");
            break;  // break; を入れないと後の処理もすべて実行してしまうので、breakでswitch文のブロックから抜ける

        case 2:
            printf("This is B");
            break;
        case 3:
            printf("This is C");
            break;

        default:  // else的なやつ。どれにも当てはまらなかった際に実行される
            printf("Please input number in 1-3");
            break;
    }

    return 0;
}