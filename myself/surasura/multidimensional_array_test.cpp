#include <stdio.h>


// 多次元配列のテスト
// 配列を地図と見立ててる
// 「スラスラわかるC言語」p192
int main(void){

    int a[3][3];  // 3*3の二次元配列
    int i, j;

    a[0][0] = 5;
    a[0][1] = 5;
    a[0][2] = 5;
    a[1][0] = 5;
    a[1][1] = 1;
    a[1][2] = 4;
    a[2][0] = 5;
    a[2][1] = 3;
    a[2][2] = 2;

    // 地図を表示する
    for (int i=0; i<=2; i++){
        for (int j=0; j<=2; j++){
            printf("%d",a[i][j]);
        }
        printf("\n");
    }

    printf("あなたはどのへんにいますか？\n");
    printf(" 0: 北の方, 1:真ん中, 2: 南の方\n");
    scanf("%d",&i);

    printf("あなたはどのへんにいますか\n");
    printf(" 0: 西の方, 1: 真ん中, 2: 東の方\n");
    scanf("%d",&j);

    // switch case文で対応したメッセージを出力
    switch(a[i][j]){
        case 1:
            printf("お城にいます\n");
            break;

        case 2:
            printf("城下町にいます\n");
            break;

        case 3:
            printf("草原にいます\n");
            break;

        case 4:
            printf("沼にいます\n");
            break;

        case 5:
            printf("ツンドラ地帯にいます\n");
            break;

        default:
            printf("どこにいるかわかりません...\n");
            break;
    }

    return 0;
}