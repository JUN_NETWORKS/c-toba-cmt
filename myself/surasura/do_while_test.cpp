#include <stdio.h>

int main(void){
     int a = 7;
     int b = 0;
     printf("数字あてゲーム\n");
     printf("整数を入力\n");

     // do ~ while文は、while文と違って、1度実行してから条件式を参照してその後繰り返すか決める
     do{
         scanf("%d",&b);
         printf("ハズレです"); // 正解してもハズレと表示されるのでこのプログラムにdo while は合っていない
     }while(a!=b);

     printf("正解です");

     return 0;
}