// multi array test
#include <stdio.h>

int main(void){
  
  // make multi array
  int num[2][5] = {{1,3,5,7,9},{2,4,6,8,10}};

  // sum
  for (int i=0;i<2;i++){
    for (int j=0;j<5;j++){
      printf("[%d][%d]: %d\n",i,j,num[i][j]);
      }
    }

  return 0;
  }
