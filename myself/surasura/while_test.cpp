#include <stdio.h>


// 数字を当てるまでwhileで繰り返すプログラム
int main(void){
    int a = 10;
    int b = 0;
    printf("数字当てるまで繰り返す\n");
    printf("整数を入力\n");
    scanf("%d",&b);

    while(a!=b){
        printf("ハズレです。\n");
        scanf("%d",&b);
    }

    printf("正解です。\n");

    return 0;
}