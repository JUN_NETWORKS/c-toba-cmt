#include <stdio.h>

int main(void){
    int a;

    printf("繰り返す回数を入力してください\n");
    scanf("%d",&a);

    for(int i=1;i<=a;i++){ // for(初期設定;繰り返し条件;1ループごとの処理)
        printf("%d\n",i);
    }

    return 0;
}