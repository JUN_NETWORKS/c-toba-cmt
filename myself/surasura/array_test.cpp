#include <stdio.h>

// 配列のテスト
int main(void){

    int test[5] = {10,20,30,40,50}; // 変数名[配列の数] = {配列の数まで入れることができる}  // 配列数より少ない場合は 0 で穴埋めされる

    for (int i=0;i<=4;i++){

        printf("test[%d]: %d\n",i,test[i]);
    
    }

    // 配列のコピー
    int b[5];
    for (int i=0;i<=4;i++){  // いっぺんには代入できないのでforで一つずつ入れる
        b[i] = test[i];
        printf("b[%d]: %d\n",i,b[i]);
    }

    return 0;
}