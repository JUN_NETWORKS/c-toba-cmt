using std::cout;
using std::endl;
using std::cin;

class Board{
    public:
        int board[3][3] = {0};
        int player_turn = 1;  // o: 1, x: -1
        bool gameEnded = false;
        int winner;

        // 行動する関数
        void take_action(int x, int y){

            board[x][y] = player_turn;

            player_turn *= -1;

            // 勝敗判定
            winner = check_winner();
            if (winner!=0){
                gameEnded = true;
                return;
            }

            // 全部のマスが埋まっていて勝者がいなければ引き分け
            for (int i=0; i<3; i++){
                for (int j=0; j<3; j++){
                    // 空きマスが一つでもあればゲーム続行
                    if (board[i][j]==0){
                        return;
                    }
                }
            }

            gameEnded = true;

        }

        int check_winner(){
            // 横の判定
            for (int i=0; i<3; i++){
                if (board[i][0]==board[i][1]&&board[i][1]==board[i][2]){
                    return board[i][0];
                }
            }

            // 縦の判定
            for (int j=0; j<3; j++){
                if (board[0][j]==board[1][j]&&board[1][j]==board[2][j]){
                    return board[0][j];
                }
            }

            // 斜めの判定
            if (board[0][0]==board[1][1]&&board[1][1]==board[2][2])
                return board[0][0];
            if (board[0][2]==board[1][1]&&board[1][1]==board[2][0])
                return board[0][2];

            // 引き分け
            return 0;
        }

        // 画面表示
        void print_board(){
            for (int i=0; i<3; i++){
                cout << "| ";
                for (int j=0; j<3; j++){
                    if (board[i][j]==1)
                        cout << " o ";
                    else if(board[i][j]==-1)
                        cout << " x ";
                    else
                        cout << "   ";

                    cout << "|";
                }        
                cout << endl;        
            }
        }
};