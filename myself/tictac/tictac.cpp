#include<iostream>
#include"Board.h"

using namespace std;

main(int argc, char const *argv[])
{

    int x,y;

    Board board;

    // 初期状態を表示
    system("clear");
    board.print_board();

    while(!board.gameEnded){
        if (board.player_turn==1)
            cout << "White turn: ";
        else
            cout << "Black turn: ";
        
        scanf("%d,%d",&x,&y);

        board.take_action(x, y);

        system("clear");
        board.print_board();
    }

    if (board.winner==1)
        cout << "White win!!" << endl;
    else if (board.winner==-1)
        cout << "Black win!!" << endl;
    else
        cout << "Draw!!" << endl;

    return 0;
}
