#include <stdio.h>

int sort(int x[]){
    if(x[0]>x[1]){
        return 0;
    }else{
        int tmp=x[0];
        x[0] = x[1];
        x[1] = tmp;
        return 0;
    }
}

int main(void){
    int A[2];
    printf("1つ目: "); scanf("%d", &A[0]);
    printf("2つ目: "); scanf("%d", &A[1]);

    sort(A);

    printf("大きい値: %d\n",A[0]);
    printf("小さい値: %d\n",A[1]);

    return 0;
}
