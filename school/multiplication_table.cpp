#include <stdio.h>


// 九九を出力するプログラム
int main(void){
    for(int i=1; i<=9; i++){
        for(int j=1;j<=9;j++){
            printf("%3d  ",j*i);  // %3dで数値の出力幅を3文字にする
        }
        printf("\n");
    }

    return 0;
}