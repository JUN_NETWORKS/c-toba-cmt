#include<iostream>

using namespace std;

float calc_default(int height){
    return (height-100)*0.9;
}

main(int argc, char const *argv[])
{
    int start, end, range;

    cout << "何cmから: ";
    cin >> start;
    cout << "何cmまで: ";
    cin >> end;
    cout << "何cmごと: ";
    cin >> range;

    cout << "----------------------" << endl;

    int current_height = start;
    float weight;

    while (current_height <= end){
        weight = calc_default(current_height);
        cout << current_height << "cm " << weight << "kg" << endl;

        current_height += range;
    }

    return 0;
}
