#include <stdio.h>

// 3つの入力から最大値を出力するプログラム

// ２つの引数を取って大きい方を返す関数
int max(int a, int b){
    int max;
    if (a>b)
        return a;
    else
        return b;
}

int main(void){
    int a,b,c,max_num;

    printf("A:"); scanf("%d",&a);
    printf("B:"); scanf("%d",&b);
    printf("C:"); scanf("%d",&c);

    max_num = max(a,b);
    max_num = max(max_num,c);

    printf("%d\n",max_num);

    return 0;
}