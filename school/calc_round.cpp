#include<iostream>

using namespace std;

main(int argc, char const *argv[])
{
    float d, h, S;
    float pi = 3.14159;
    
    cout << "円柱の底辺d: ";
    cin >> d;
    cout << "円柱の高さh: ";
    cin >> h;
    S = pi*d*h+(pi*d*d/2);
    cout << "円柱の表面積S: " << S << endl;
    
    return 0;
}
