#include<stdio.h>

int kaijo(int num){
    int ans = 1;
    for(int i=1; i<=num; i++){
        ans *= i;
    }
    return ans;
}

int main(int argc, char const *argv[])
{
    
    int n;
    printf("値を入力: ");
    scanf("%d", &n);
    int ans = kaijo(n);
    printf("%dの階乗: %d\n", n, ans);

    return 0;
}
