#include<stdio.h>

typedef struct student {
    char name[64];
    int gender;
    int phys, english;
} info;

int main(void)
{
    info You;
    
    printf("\n------あなたの情報の入力------\n");

    printf("名前: ");
    scanf("%s",You.name);
    printf("\n性別男なら[1], 女なら[0]: ");
    scanf("%d",&You.gender);
    printf("\n物理の成績: ");
    scanf("%d",&You.phys);
    printf("\n英語の成績: ");
    scanf("%d",&You.english);


    printf("\n------性別の表示------\n");
    if (You.gender==1){
        printf("\n[%s]は男性です", You.name);
    }else{
        printf("\n[%s]は女性です", You.name);
    }

    printf("\n------点数の表示------\n");
    printf("\n[%s]の物理: %d", You.name, You.phys);
    printf("\n[%s]の英語: %d", You.name, You.english);

    printf("\n\n");

    if(You.phys > You.english){
        printf("\n[%s]は物理が高かったです", You.name);
    }else if(You.phys < You.english){
        printf("\n[%s]は英語が高かったです", You.name);
    }else{
        printf("\n[%s]は物理と英語は同得点でした", You.name);
    }

    return 0;
}
