#include<stdio.h>
int main( void )
{
    FILE *f; //FILE構造体のアドレス
    char MyName[64];
    int No, Salary;
    f = fopen( "test.txt", "r" );
    if (f==NULL){
        printf("そんなファイルないぞ");
        return 1;
    }
    printf( "ファイルから読み出します。\n\n" );
    fscanf( f, "%s", MyName );
    fscanf( f, "%d", &No );
    fscanf( f, "%d", &Salary ); //出席番号の読み込み

    printf("読み出した情報を表示します。\n");
    printf("---------------------------\n");

    printf("\n名前: %s", MyName);
    printf("\n出席番号: %d", No);
    printf("\n希望初任給: %d", Salary);

    printf("\n---------------------------\n");
    if(Salary>=230000){
        printf("意識高いっすね");
    }else if(230000>Salary>=210000){
        printf("高みがある");
    }else if(210000>Salary>=190000){
        printf("ぷるすうるとら");
    }else if(190000>Salary>=170000){
        printf("がんばれ");
    }else if(170000>Salary>=150000){
        printf("人生楽しく");
    }else if(150000>Salary>=130000){
        printf("頑張って");
    }else{
        printf("悲しいこと言わないで");
    }

    fclose(f);

    return 0;
}