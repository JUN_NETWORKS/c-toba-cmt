#include<stdio.h>


int main(int argc, char const *argv[])
{
    // float型の変数
    float A,B;
    // float型のポインタ
    float *pA, *pB;

    // 変数Aのアドレスを代入
    pA = &A;
    pB = &B;

    printf("変数Aのアドレス: %p\n", &A);
    printf("変数Bのアドレス: %p\n", &B);
    printf("ポインタpAのアドレス: %p\n", &pA);
    printf("ポインタpBのアドレス: %p\n", &pB);

    return 0;
}
