#include <stdio.h>

float power(float a, int n){
    float ans = 1;
    int n2 = n<0? n*-1: n;
    for (int i=1; i<=n2; i++){
        ans = n > 0? ans*a: ans/a;
    }
    return ans;
}

int main(int argc, char const *argv[])
{
    float a;
    int n;
    
    printf("aをn回累乗した結果を表示します。\n");
    printf("a(実数): ");
    scanf("%f",&a);
    printf("n(整数): ");
    scanf("%d", &n);
    float ans = power(a, n);
    printf("aのnの累乗は%fです。\n", ans); 
    return 0;
}
