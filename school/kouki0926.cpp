#include <stdio.h>
void sort(int *X, int *Y)
{
  int tmp;
  if(*X==*Y){
    return;
  }

  if(*X<*Y){
    tmp = *X;
    *X = *Y;
    *Y = *X;
  }
}
int main(void)
{
int A, B;
printf("一つ目:"); scanf("%d", &A);
printf("二つ目:"); scanf("%d", &B);

sort(&A, &B); //並び替え
printf("大きい値：%d\n", A);
printf("小さい値：%d\n", B);

return 0;
}
