#include<stdio.h>

int main(void){

  int score[3][2];

  // 各生徒の成績を入力
  printf("3名の成績を入力してください。\n");
  for (int i=0; i<3; i++){
    for (int j=0; j<2; j++){
      if (j==0){
        printf("【%d人目】数学のテストの点数: ", i+1);
      }else{
        printf("【%d人目】物理のテストの点数: ", i+1);
      }
      scanf("%d",&score[i][j]);
      printf("\n");
    }
  }
  printf("--------------------\n");

  // 3人の各成績を合計点と平均点を出す
  printf("3名の成績を出力します。\n");
  // 3人の合計点を入れる配列
  int sum_score[3];
  for (int i=0; i<3; i++){
    // 科目ごとの点数出力
    for (int j=0; j<2; j++){
      if (j==0){
        printf("【%d人目】数学のテストの点数: %d点\n", i+1, score[i][j]);
        sum_score[i]+=score[i][j];
      }else{
        printf("【%d人目】物理のテストの点数: %d点\n", i+1, score[i][j]);
        sum_score[i]+=score[i][j];
      }
    }

    //合計点と平均点と合否
    printf("【%d人目】合計点: %d\n",i+1, sum_score[i]);
    printf("【%d人目】平均点: %.1f\n",i+1, (float)sum_score[i]/2);  // (float)でfloatがたに変換
    if ((float)sum_score[i]/2 < 60){
      printf("【%d人目】勉強しましょう\n", i+1);
    }

    printf("\n");
  }

  return 0;
}
