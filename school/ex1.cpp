// 追加設問 ex1

#include <stdio.h>
#include <math.h>
#include <iostream>

// 平方根を求める関数
float f_sqrt(float x){
    return (float)sqrt(x);
}

using namespace std;

int main(void){
    float a, b, c, d, x1, x2;

    a = 1.0;
    b = 12345.6;
    c = 0.05;

    float a1 = b*b-4.0*a*c;
    float a2 = f_sqrt(b*b-4.0*a*c);
    std::cout << a2 << std::endl;

    x1 = (-b + f_sqrt(b*b-4.0*a*c)) / (2.0*a);
    x2 = (-b - f_sqrt(b*b-4.0*a*c)) / (2.0*a);

    std::cout << "x1 = " << x1 << std::endl;
    printf("x2 = %e\n", x2);
    
    return 0;
}