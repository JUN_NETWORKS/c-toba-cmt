#include<iostream>

using namespace std;

class Rect{
    public:
        float S;  // 面積
        float x;  // 横の長さ
        float y;  // 縦の長さ

        float calc_S(){
            S = x*y;
        }
};

main(int argc, char const *argv[])
{

    float x,y;

    Rect measure;

    cout << "横の長さx: ";
    cin >> measure.x;
    cout << "縦の長さ: ";
    cin >> measure.y;

    measure.calc_S();

    cout << "面積S: " << measure.S << endl;

    return 0;
}
