//英文字を入力するとその内容をそのまま返答する

#include <stdio.h>

int main(void){

    char a[100];

    printf("英文字を入力してください: ");
    scanf("%s",a);
    printf("\nあなたはキーボードで[%s]と入力しました。\n",a);

    return 0;
}