#include <iostream>

using namespace std;

float pi = 3.14;

class Sphere {
    public:
        float r;
        float V;
        float S;

        void set_r(){
            cout << "球の半径r: ";
            cin >> r;
        }

        void calc_VS(){
            V = 4.0/3.0 * pi * r * r * r;
            S = 4 * pi * r * r;

            cout << "球の体積V: " << V << endl;
            cout << "球の表面積S: " << S << endl;
        }
};

main(int argc, char const *argv[])
{
    class Sphere Obj;
    
    Obj.set_r();
    Obj.calc_VS();

    return 0;
}
