#include<stdio.h>

float max_f(float n[]){
    // 3項演算子
    float max = n[0] > n[1]? n[0]: n[1];
    max = max > n[2]? max: n[2];
    return max; 
}

int main(int argc, char const *argv[])
{
    float num_array[3];
    for(int i=0;i<3;i++){
        printf("\n%d個目の数を入力してください: ",i+1);
        scanf("%f",&num_array[i]);
    }
    printf("\n------------------------\n");

    float max = max_f(num_array);

    printf("最大値: %f\n", max);

    return 0;
}
