// 平均を計算して、そこから偏差を計算するプログラム
#include<stdio.h>

float deviation_calc(float n[]){
    float ave, sum;
    for(int i=0; i<5; i++){
        sum+=n[i]; // 合計値
    }
    ave = sum / 5;
    printf("\n------------------------\n");
    printf("平均: %f",ave);
    printf("\n------------------------\n");
    for(int i=0;i<5;i++){
        n[i] = n[i] - ave;
        printf("%d個目の値の偏差: %f\n",i+1,n[i]);
    }
}

int main(int argc, char const *argv[])
{
    float n[5];
    // float n[5] = {1,2,3,4,5};
    for(int i=0;i<5;i++){
        printf("\n%d個目の値: ",i+1);
        scanf("%f",&n[i]);
    }

    deviation_calc(n);

    return 0;
}
