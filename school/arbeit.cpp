#include<iostream>

using namespace std;

class Arbeit{
    public:
        int salary; // 給料[円]
        float hour; // 勤務時間
        float HourlyRate; // 時給[円/時間]

        void calc_hourly_rate(){
            HourlyRate = salary/hour;
        }
};

main(int argc, char const *argv[])
{
    Arbeit arbeit;

    cout << "バイトの給料[円]: ";
    cin >> arbeit.salary;
    cout << "勤務時間[時間]: ";
    cin >> arbeit.hour;

    arbeit.calc_hourly_rate();

    cout << "あなたの時給: " << arbeit.HourlyRate << "[円]" << endl;

    if (arbeit.HourlyRate < 846)
        cout << "最低賃金以下です。" << endl;
    else if (arbeit.HourlyRate >= 846 &&arbeit.HourlyRate <= 976)
        cout << "良いんじゃない?" << endl;
    else
        cout << "とても良い!!" << endl;

    return 0;
}
