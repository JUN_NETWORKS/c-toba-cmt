// 多次元行列の総和
#include <stdio.h>

int main(void) {

  int num[2][5] = {{1,2,3,4,5},{6,7,8,9,10}};

  int sum_num = 0;

  for (int i=0; i<2; i++){
    for (int j=0; j<5; j++){
      sum_num+=num[i][j];
    }
  }

  printf("合計は: %d です。\n", sum_num);

  /* code */
  return 0;
}
