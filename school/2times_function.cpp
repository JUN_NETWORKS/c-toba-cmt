#include <stdio.h>

// 最大値値を取り出す関数
float maxof2times(float x, float y){
    // 3項演算子
    float max = x > y? x: y;
    return max*2 ; 
}

int main(int argc, char const *argv[])
{
    float x,y;
    
    printf("2この整数を入力してください。\n");
    printf("1個目: ");
    scanf("%f",&x);
    printf("2個目: ");
    scanf("%f", &y);
    float ans = maxof2times(x,y);
    printf("大きい値の2倍は%fです。\n", ans); 
    return 0;
}
