#include <stdio.h>


int main( void )
{
    FILE *f; //MyData.txt用
    char MyName[64]; //名前用
    int No; //出席番号
    int Salary; //月収

    printf( "あなたの名前：" );
    scanf( "%s", MyName);

    printf("\n出席番号: ");
    scanf( "%d", &No);

    printf("\n希望初任給(円): ");
    scanf( "%d", &Salary);

    f = fopen("test.txt", "w");  // w: 上書き, a: 追記, r: 読み取り専用

    fprintf(f, "%s\n%d\n%d\n",MyName,No,Salary);

    fclose(f);

    return 0;

}
