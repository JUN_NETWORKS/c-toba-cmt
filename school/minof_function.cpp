#include <stdio.h>

// 最小値を取り出す関数
int minof(int x, int y, int z){
    // 3項演算子
    int min = x < y? x: y;
    min = min < z? min: z;
    return min; 
}

int main(int argc, char const *argv[])
{
    int x,y,z;
    
    printf("3この整数を入力してください。\n");
    printf("1個目: ");
    scanf("%d",&x);
    printf("2個目: ");
    scanf("%d", &y);
    printf("3個目: ");
    scanf("%d", &z);
    
    int min = minof(x,y,z);

    printf("最も小さい値は%dです。\n",min);

    return 0;
}
