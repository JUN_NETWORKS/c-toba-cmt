// 2018-04-27 の授業プリント14番のやつ

#include <stdio.h>

int main(void){
    int a[5] = {1,3,5,7,9};
    int b[5];

    // 配列のコピー
    for (int i=0;i<=4;i++){  // いっぺんには代入できないのでforで一つずつ入れる
        b[i] = a[i];
        printf("b[%d]: %d\n",i,b[i]);
    }

    return 0;
}