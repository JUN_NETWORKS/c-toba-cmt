#include<iostream>

using namespace std;

main(int argc, char const *argv[])
{
    int month;

    cout << "誕生月を入力してください: ";
    cin >> month;

    if (3<=month && month<=5){
        cout << "誕生日は春です" << endl;
    }else if(6<=month && month<=8){
        cout << "誕生日は夏です" << endl;
    }else if(9<=month && month<=11){
        cout << "誕生日は秋です" << endl;
    }else if((1<=month && month<=2) || month==12){
        cout << "誕生日は冬です" << endl;
    }else{
        cout << "入力された月は存在しません。" << endl;
    }

    return 0;
}
