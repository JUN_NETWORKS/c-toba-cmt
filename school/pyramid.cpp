#include <stdio.h>


// ピラミッドを標準出力するプログラム
int pyramid(int num){
    for (int i=1; i<=num; i++){
        for(int j=1; j<=num-i; j++){
            printf(" ");
        }
        for(int z=1; z<=2*i-1; z++){
            printf("*");
        }
        printf("\n");
    }
    return 0;
}

int main(void){
    int a;
    printf("ピラミッドの段数\n");
    scanf("%d",&a);

    pyramid(a);

    return 0;
}