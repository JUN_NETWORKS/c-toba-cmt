// 2018-04-27 の授業プリント10番のやつ

#include <stdio.h>

// 配列
int main(void){
    int score[10];

    for (int i=0;i<=9;i++){
        printf("%d人目の点数を入力してください\n",i+1);
        scanf("%d",&score[i]);
    }

    for (int i=0;i<=9;i++){
        if (score[i] >= 60){
            printf("%d番の人は合格です。\n",i+1);
        }else{
            printf("%d番の人は不合格です。\n",i+1);
        }
    }

    float ave = 0;
    for (int i=0;i<=9;i++){
        ave+=score[i];
    }

    printf("平均点は%.2fです。\n",ave/10);

    return 0;
}