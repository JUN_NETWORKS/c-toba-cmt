#include<stdio.h>

int X;

int func(int a){
    X = X*(2*a);

    return 2*a;
}

int main(int argc, char const *argv[])
{
    int a=1, b=2;
    X=3;

    printf("%3d %3d %3d\n", a, b, X);
    b = func(a);
    printf("%3d %3d %3d\n", a, b, X);
    a = func(b);
    printf("%3d %3d %3d\n", a,b,X);
    return 0;
}
