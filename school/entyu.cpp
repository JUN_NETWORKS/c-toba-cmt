#include<iostream>

using namespace std;

float pi = 3.14159;

class Data{
    public:
        float d;  // 底辺の長さ
        float h;  // 円柱の高さ

        // 表面積を返す
        float calc_S(){
            return pi*d*d/2+pi*d*h;
        }

        // 体積を返す
        float calc_V(){
            return pi*d*d*h/4;
        }

        // 表面積と体積を表示
        void print_S_V(){
            cout << "表面積S: " << calc_S() << endl;
            cout << "体面積V: " << calc_V() << endl; 
        }
};

main(int argc, char const *argv[])
{
    Data data;

    cout << "底辺の長さd: ";
    cin >> data.d;
    cout << "高さh: ";
    cin >> data.h;

    data.print_S_V();

    return 0;
}
