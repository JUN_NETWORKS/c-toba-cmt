#include <stdio.h>

main(int argc, char const *argv[])
{
    FILE *f;  // FILEの構造体アドレス
    f = fopen("test.txt", "w");  // w: 上書き, a: 追記, r: 読み取り専用

    printf("今日の日付を書き込みます");
    fprintf(f, "今日は%d月%d日です\n",11,21);

    fclose(f);

    return 0;
}
