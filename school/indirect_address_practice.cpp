#include<stdio.h>

int main(int argc, char const *argv[]) {

  int a,b,c;
  int *p, *q, *r;

  p = &a;
  q = &b;
  r = &c;

  *p = 5;
  *q = 6;
  *r = *p+*q;

  printf("a: %d b: %d c: %d \n", *p, *q, *r);

  return 0;
}
