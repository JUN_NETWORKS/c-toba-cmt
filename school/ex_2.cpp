#include<iostream>
#include<random>
#include<vector>

using namespace std;

int map_generate(){
    // 乱数生成器
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> map_rd(1,10);

    int map[20][20];
    for(int i=0;i<20;i++){
        for (int j=0;j<20;j++){
            map[i][j] = map_rd(mt) <= 2? 1: 0;
        }
    }

    return map;
}

int main(int argc, char const *argv[])
{
    int *map;
    map  = map_generate();

    for(int i=0;i<20;i++){
        for(int j=0;j<20;j++){
            if (map[i][j]==1){
                std::cout << "○" << " ";
            }else{
                std::cout << "×" << " ";
            }
        }
        std::cout << "\n";
    }

    return 0;
}
