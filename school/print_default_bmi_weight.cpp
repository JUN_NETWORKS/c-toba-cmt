#include<iostream>

using namespace std;

void print_bmi_category(float bmi){
    if (bmi < 18.5)
        cout << "低体重(痩せ型)です。" << endl;
    else if (bmi >= 18.5 && bmi < 25)
        cout << "普通体重です。" << endl;
    else if (bmi >= 25 && bmi < 30)
        cout << "肥満(1度)です。" << endl;
    else if (bmi >= 30 && bmi < 35)
        cout << "肥満(2度)です。" << endl;
    else if (bmi >= 40 && bmi < 40)
        cout << "肥満(3度)です。" << endl;
    else
        cout << "肥満(4度)です。" << endl;
}

main(int argc, char const *argv[])
{
    float height, weight;
    float BMI, appropriate_weight;

    cout << "身長[cm] = ";
    cin >> height;
    cout << "体重[kg] = ";
    cin >> weight;

    height /= 100;  // [cm] => [m]

    cout << "---------------------------" << endl;

    BMI = weight/(height*height);
    cout << "BMI = " << BMI << endl;
    print_bmi_category(BMI);

    cout << "---------------------------" << endl;

    appropriate_weight = (height*height)*22;

    cout << "適正体重 = " << appropriate_weight << "[kg]" << endl;
    if (weight >= appropriate_weight)
        cout << "適正体重より " << weight-appropriate_weight << "[kg]重いです" << endl;
    else
        cout << "適正体重より " << appropriate_weight-weight << "[kg]軽いです" << endl;

    return 0;
}
